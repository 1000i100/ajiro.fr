---
date: 2018-11-15
draft: true
title: "Vous saurez tout sur le Coaching Craft"
duration: 1H
genre: Présentation
authors:
  - benoist_alexis
tags:
  - Software Craftsmanship
  - concepts
  - conference
illustration:
  name: cathedrale
  source: https://www.flickr.com/photos/beta_karel/6745232529/in/photolist-bh45Cz-hrfBfB-VZiXTN-5E6PFv-29TkLTA-WE5xiy-W2RUk8-9ZAXDh-ghgMD5-4dgYDr-VZj3fo-XciL73-5f7uCg-XfUkyg-2buopy2-EeVyvP-aJT8bt-ePHShS-oy1fK1-aJST9B-QHhmn3-aUmKoK-oziqkN-oLp6Ub-bEQtie-2cNJ9sb-2cT7Hck-2cNJqz1-P7ZX2e-oSJQ4s-jBWrWc-23FhYQQ-jZCWhx-oeQmbX-2buoDSn-51rBdW-oSuETa-2bM5EVs-2cNFrYu-eGgqZi-2bupK28-5norty-9f79K4-bqExw3-oy1bxd-HS7zJP-Gex5xB-63Veb3-2cNHrzu-aZ5GBg
abstract: |
  Martin Fowler cite l'excellence technique comme
  un des trois challenges pour l'agilité en 2018.
  Le coaching craft vise à accompagner les équipes vers cette excellence à la fois dans le savoir faire et le savoir être.
  En plus de chercher à caractériser ce qu'est le coaching craft, nous l'illustrerons avec nos retours d'expérience chez nos clients.
---

# Présentation détaillée

Ce talk s'addresse aux développeurs, aux product owners, aux scrum master, aux manageurs, en bref toute personne en contact de près ou de loin avec une équipe de développement.
Vous aurez une idée plus précise de ce qu'est le coaching craft:

* À quoi ça sert ?
* Qu'est-ce que c'est ?
* Comment ça se passe ?
* Combien de temps ça prend ?
* Qu'est-ce qu'un coach craft ne fait pas ?

Et vous aurez quelques éléments pour commencer à mettre en pratiques cette recherche de l'excellence dans votre équipe !

# But de la session

Faire découvrir le coaching craft aux équipes, dev

# Déroulement de la session

# Tweet

# Sources d'inspirations

* [State of agile in 2018](https://martinfowler.com/articles/agile-aus-2018.html)
