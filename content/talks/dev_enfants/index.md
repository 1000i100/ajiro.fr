---
date: 2018-09-10
draft : true
title: "Apprendre les bases du code à nos enfants"
duration: 2 h
genre: Atelier
authors:
  - clavier_celeste
tags:
  - développement
  - enfants
  - code
illustration:
  name: collaboration
  source: Thomas Clavier
abstract: |
  Venez vous mettre à la place d'enfants pour apprendre comment apprendre l'algorithmique aux enfants à travers un atelier de deux heure sans clavier.
---

# Présentation détaillée

Vous avez déjà essayer d'apprendre l'algorithmique à un enfant de 6 ans ? Moi oui ! Rentrez dans la peau d'un enfant qui apprend à coder ... sans clavier.

L'année dernière avec mon papa, nous vous avons raconté les coding gouter et vous avez adorés ! Et d'après vos retours il nous a semblé que vous étiez prêts pour un peu de pratique. Nous voilà donc avec deux heures de pratiques pour les grands qui savent se prendre pour des petits, pour les développeurs qui savent réapprendre les bases, mais aussi pour les vrais petits et les vrais non-développeurs.

Au programme : dady robot, potato pirates et potion magique.

# Tweet

Vous avez déjà essayer d'apprendre l'algorithmique à un enfant de 6 ans ? Venez apprendre en vous mettant à leur place.
