---
date: 2018-10-17
title: "Vivre le Behavior Driven Development (BDD)"
duration: 2H
genre: Atelier
draft: true
authors:
  - clavier_thomas
tags:
  - Software Craftsmanship
  - bdd
  - concepts
  - atelier
  - conference
illustration:
  name: bdd
abstract: |
  Cet atelier présente le Behavior Driven Development (BDD) et propose d'apprendre en faisant. À travers un cas concret vous pratiquerez la conception et la rédaction collaborative de scénarios BDD.
  Cet atelier donnera l'envie d'approfondire le sujet afin de généraliser l'écriture de tests d'acceptance de haute qualité ou plus précisément, de "spécifications exécutables", pour écrire un code mieux conçu, plus facile à maintenir et plus fiable.
  
  Pas la peine de savoir coder ! L'atelier est tourné sur la discussion, la construction de scénarios et la rédaction de spécifications par l'exemple.
  Chers gens du métier, manager ou currieux, venez découvrir le monde du BDD.
---

## But de la session

Découvrir quelques bonnes pratiques du génie logiciel : TDD, Clean Code, Refactoring, Dette Technique et Integration Continue sont au programme !

## Disposition de la pièce

Une grande salle avec des ilots de 6 personnes.

## Déroulement de la session

À travers 4 exercices à réaliser en légo, nous aborderons quelques-unes des bonnes pratiques du développement logiciel.

Le déroulement de chaque exercice est le suivant :

- découverte du sujet
- construction
- rétrospective sur l'exercice
- présentation de la théorie du génie logiciel découverte durant l'exercice avant de l'appliquer durant les autres exercices.

# Tweet

Découvrir quelques bonnes pratiques du génie logiciel : TDD, Clean Code, Refactoring, Dette Technique et Integration Continue sont au programme ! Comment ? Avec de l'imagination, des rires et des Legos !
