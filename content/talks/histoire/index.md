---
date: 2018-12-02
title: "L'informatique pop culture, ne va pas se revivre"
duration: 1H
draft: True
genre: Présentation
authors:
  - benoist_alexis
tags:
  - concepts
abstract: |
  Que peut-on apprendre de l'histoire de l'informatique ?
---

# Présentation détaillée

Histoire de la gestion de projets informatiques

Si on ne connait pas l'histoire, on la revivra.

Conclusion: Les méthodes de développement sont une construction sociale et non technique.

## Horde mongole

Premier projet avec l'armée américaine.
? Quel projet ?

On embauche des hordes de programmeurs car c'est plus facile de justifier le coût par rapport au client.

Les manageurs sont content car ils managent des gens sans expertise technique et ils ne posent pas trop de problèmes.

Ils sont frusté mais ils n'ont pas le pouvoir de changer les choses.

Il y a toujours un compromis entre l'autonomie et la coordination.

Il y a une système hiérarchique au dessus de tout cela pour coordonner.

La croyance qui impose cette pratique c'est qu'on peut découper un système logiciel en un ensemble de modules. Ensuite, il suffit de coordonner le développement des modules.


## Chief Programmer Team

C'est un modèle qui est concentré autour d'un programmeur en chef, qui travaille à la manière d'un chirugien.

Il est assisté d'un:
 * assistant programmeur (senior dev aussi, qui discute challenge)
 * language advocate
 * ? assistant administratif
 * PMO
 *


? Qui l'a utilisé ?

Permet d'avoir de très bons résultats en très peu de temps.

Il diminue le nombre de communications nécessaires, grace à la structure en étoile avec le CPT.

Peu acceptable socialement:
 * les manageurs n'aiment pas le fait de dépendre uniquement de deux individus qui ont une relation très proche
 * Peu de développeurs ont la compétence et l'énergie nécessaire pour assurer ce role

## Equipe auto gérée

Modèle de gerald Weinberg (psychology of computer programming)

? Vrai nom ?

Plait aux développeurs mais réduit le pouvoir du management et les développeurs ont eu tardivement la chance de le gérer.

# Elaboration de compétences de base

Elaboration du CS

Est-ce que c'est une science ?
Dur à faire accepter par l'université

Ama de connaisances (algos, complexité)

Peu de liens avec le monde du developpeur commercial.

Naissance du software engineering

Software Craftsmanship


# Problème du recrutement
